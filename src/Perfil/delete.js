//@flow
import React from 'react';
import { Mutation } from 'react-apollo';
import { PERFILESQUERY, DELETE_PERFIL } from './queries';
import { Button } from '@material-ui/core';
import Alumnos from '../alumnos';

const DeleteAlumno = (id) => {
  return <div> 
      <Mutation mutation={DELETE_PERFIL}>
            {(deleteAlumnos, { data }) => {
                function deletePerfil() {
                    deleteAlumnos({
                        variables: id, refetchQueries: [{ query: PERFILESQUERY }] 
                       })
                       return Alumnos();
                }
               return <Button variant="contained" color="secondary" onClick={() => deletePerfil()}>Eliminar</Button>
            }}
        </Mutation>
  </div>;
};

export default (DeleteAlumno);
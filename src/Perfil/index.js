//@flow
import React from 'react';
import { Query} from 'react-apollo';
import { PERFILESQUERY} from './queries';
import DeleteAlumno from './delete';
import { Typography, Grid } from '@material-ui/core';
import { Row } from '../alumnos/styledCompontes';

const Perfil = () => {
    return <div>
            <Typography variant="h4"><strong>PERFIL</strong></Typography>
            <Grid container style={{marginBottom: '1%', backgroundColor: '#F2F2F2'}}>
                <Grid container item xs={1} justify="center"><Typography>id</Typography></Grid>
                <Grid container item xs={1} justify="center"><Typography>UsuarioId</Typography></Grid>
                <Grid container item xs={1} justify="center"><Typography>GradoId</Typography></Grid>
                <Grid container item xs={1} justify="center"><Typography>GrupoId</Typography></Grid>
                <Grid container item xs={1} justify="center"><Typography>EscuelaId</Typography></Grid>
            </Grid>
        <Query query={PERFILESQUERY}>
            {({ loading, error, data }) => {
                if (loading) return "Loading...";
                if (error) return (console.log(error));
                if (data) {
                    return data.perfil.map((element) => (
                        <Row container key={element.id} style={{marginBottom: '1%'}}>
                            <Grid container item xs={1} justify="center" alignItems="center"><Typography key={`${element.id}`}>{element.id}</Typography></Grid>
                            <Grid container item xs={1} justify="center" alignItems="center"><Typography key={`${element.usuario_id}`}>{element.usuario_id}</Typography></Grid>
                            <Grid container item xs={1} justify="center" alignItems="center"><Typography key={`${element.grado_id}`}>{element.grado_id}</Typography></Grid>
                            <Grid container item xs={1} justify="center" alignItems="center"><Typography key={`${element.grupo_id}`}>{element.grupo_id}</Typography></Grid>
                            <Grid container item xs={1} justify="center" alignItems="center"><Typography key={`${element.escuela_id}`}>{element.escuela_id}</Typography></Grid>
                            <DeleteAlumno id={element.id}/>
                        </Row>
                    ));
                }
                else {
                    return (
                        <div> get some work boi...</div>
                    )
                }
            }}
        </Query>
    </div>;
};

export default (Perfil);
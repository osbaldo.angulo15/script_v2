//@flow
import React from 'react';
import { Mutation } from 'react-apollo';
import Checkbox from '@material-ui/core/Checkbox';
import { ADD_PERFIL, ESCUELAS, GRUPO, PERFILESQUERY, GET_PERFIL_BY_USUARIOID, GET_CLAVE_DE_CENTRO_DE_TRABAJOS, ADD_ADD_DOMICILIOS } from './queries';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { Close } from '@material-ui/icons';
import { Tooltip, Typography, CircularProgress } from '@material-ui/core';

const AddPerfil = ({ id, clave, nombreGrupo, grado, disabled }) => {
    const {
        called: calledClaveCT,
        loading: loadingClaveCT,
        data: dataClaveCT,
    } = useQuery(GET_CLAVE_DE_CENTRO_DE_TRABAJOS, { variables: { clave: clave } })
    const {
        called: calledEscuela,
        loading: loadingEscuela,
        data: dataEscuela,
    } = useQuery(ESCUELAS, { variables: { clave: clave } })
    const {
        called: calledGrupo,
        loading: loadingGrupo,
        data: dataGrupo,
    } = useQuery(GRUPO, { variables: { nombre: nombreGrupo } })
    const {
        called: calledPerfil,
        loading: loadingPerfil,
        data: dataPerfil,
    } = useQuery(GET_PERFIL_BY_USUARIOID, { variables: { id: id } })
    const [addDomicilio, { data: dataDomicilio }] = useMutation(ADD_ADD_DOMICILIOS);
    const [desabilitar, setDesabilitar] = React.useState(false);
    function onChange(e) {
        if(e){
            setDesabilitar(e);
        }
    }
    return <div>
        <Mutation mutation={ADD_PERFIL}>
            {(addPerfil, { data }) => {
                if (loadingPerfil || loadingClaveCT || loadingEscuela ||
                    loadingGrupo) {
                    return <CircularProgress color="primary" />;
                } else if (dataClaveCT.escuelas.length !== 0) {
                    return <Checkbox
                        color="primary"
                        defaultChecked={dataPerfil.perfil.length !== 0}
                        disabled={dataPerfil.perfil.length !== 0 || desabilitar ? (true) : (false)}
                        onChange={(e)=>onChange(e)}
                        onClick={() => {
                            addDomicilio({ variables: { usuarioId: id, } });
                            addPerfil({
                                variables: {
                                    escuelaId: dataEscuela.escuelas[0].id,
                                    gradoId: grado,
                                    grupoId: dataGrupo.grupos[0].id,
                                    usuarioId: id,
                                }, refetchQueries: [{
                                    query: PERFILESQUERY
                                }]
                            })
                        }
                        }
                    >
                        profile
                    </Checkbox>
                } else {
                    return <Tooltip title={<Typography variant="subtitle1">La clave de centro de trabajo no se encuentra en hasura</Typography>} placement="right"><Close color="error" /></Tooltip>
                }

            }}
        </Mutation>
    </div>;
};

export default (AddPerfil);
import gql from 'graphql-tag';

export const PERFILESQUERY = gql`
query MyQuery {
 __typename
 perfil {
   centro_trabajo_adicional
   escuela_id
   grado_id
   grupo_id
   id
   usuario_id
   zona_escolar_id
 }
}
`;

export const USUARIOID = gql`
query getPerfilByUsuarioId($id: Int) {
    perfil(where: {usuario_id: {_eq: $id}})
    {
        id
    }
}
`;
export const ADD_PERFIL = gql`
mutation addPerfil(
$escuelaId: Int!,
$gradoId: Int!,
$grupoId: Int!,
$usuarioId: Int!,
) {
    insert_perfil(objects: {
        escuela_id: $escuelaId,
        grado_id: $gradoId,
        grupo_id: $grupoId,
        usuario_id: $usuarioId,
    }){
      returning{
        id
      }
    }
   }
   
`;
export const ADD_ADD_DOMICILIOS = gql`
mutation addPerfil(
$usuarioId: Int!,
) {
    insert_domicilios(objects: {calle: "0", codigo_postal: "0", colonia: "0", lat: "0", localidad: "0", lon: "0", numero_exterior: "0", numero_interior: "0", usuario_id: $usuarioId}) {
      returning{
      id
      codigo_postal
      colonia
    }
    affected_rows
  }
   }
   
`;
export const DELETE_PERFIL = gql`
mutation deletePerfil($id: Int) {
    __typename
    delete_perfil(where: {id: {_eq: $id}})
    {
      affected_rows
    }
   }
`;

export const ESCUELAS = gql`
query escuela($clave: String) {
    escuelas(where: {clave_centro_trabajo: {_eq: $clave}}) {
      id
    }
   }
`;
export const ZONAS_ESCOLARES = gql`
query zonaEscolar($clave: String) {
    zonas_escolares(where: {clave_centro_trabajo: {_eq: $clave}}) {
      id
    }
   }
`;
export const GET_PERFIL_BY_USUARIOID = gql`
query getPerfilByUsuarioId($id: Int) {
  perfil(where: {usuario_id: {_eq: $id}}) {
      id
  }
}
`;
export const GRUPO = gql`
query grupoId($nombre: String) {
    grupos(where: {nombre: {_eq: $nombre}}) {
    id
    }
   }
`;

export const GET_CLAVE_DE_CENTRO_DE_TRABAJOS = gql`
query getClaveDeCentroDeTrabajo($clave: String) {
  escuelas(where: {clave_centro_trabajo: {_eq: $clave}}) {
    id
    nombre
  }
}
`;
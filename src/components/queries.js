import gql from 'graphql-tag';

export const MARK_DONE=gql`
mutation MarkDone($id: Int!) {
   update_municipios(where: {id: {_eq: 100}}, _set: {nombre: "OSBALDO"}){
  returning{
    id
  }
}}
`;

export const TODO = gql` 
query {
  municipios{
    id
    nombre
  }
}
`
export const ADD_TODO = gql`
mutation AddTodo($todo: String!)  {
  insert_municipios(objects: {nombre: $todo}){
    returning{
      id
    }
  }
}
`;

export const DEL_TODO=gql`
mutation AddTodo($id: Int!) {

delete_municipios(where: {id: {_eq: $id}}){
  affected_rows
}
}
`;
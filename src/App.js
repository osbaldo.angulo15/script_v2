import React, { Component } from 'react';
import './App.css';
import ApolloClient from "apollo-boost";
import {ApolloProvider} from "react-apollo";
import Alumnos from "./alumnos/index";
import Perfil from "./Perfil/index";
import Domicilios from './alumnos/domicilios';


const client = new ApolloClient({
  uri: "https://api.escuela.work/v1/graphql",
  headers: {"x-hasura-admin-secret": "nJiahsjaitspuTso4"},
});


class App extends Component {
	render() {
    return (
        <ApolloProvider client={client}>
        <div className="todo-app container">
          <h1  className=" title center blue-text">USUARIOS</h1>
          <Alumnos />
          <Perfil hola={7}></Perfil>
          <Domicilios></Domicilios>
        </div>
        
        </ApolloProvider>
    );
  }
}
export default App;

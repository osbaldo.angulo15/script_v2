//@flow
import React from 'react';
import { Mutation } from 'react-apollo';
import { ALUMNOSQUERY, DELETE_ALUMNOS, DELETE_PERFIL_BY_USUARIO_ID } from './queries';
import { useMutation } from '@apollo/react-hooks';
import { Button } from '@material-ui/core';
import {DELETE_DOMICILIOS_BY_USUARIO_ID} from './domicilios/queries'

const DeleteAlumno = (id) => {
    const [addTodo, { data }] = useMutation(DELETE_PERFIL_BY_USUARIO_ID);
    const [deleteDomicilio] = useMutation(DELETE_DOMICILIOS_BY_USUARIO_ID);
    return <div>
        <Mutation mutation={DELETE_ALUMNOS}>
            {(deleteAlumnos, { data }) => (
                <Button variant="contained" color="secondary" onClick={() => {
                    addTodo({ variables: { usuarioId: id.id } });
                    deleteDomicilio({ variables: { usuarioId: id.id } });
                    deleteAlumnos({
                        variables: id, refetchQueries: [{ query: ALUMNOSQUERY }]
                    })
                }
                }>ELIMINAR</Button>
            )}
        </Mutation>
    </div>;
};

export default (DeleteAlumno);
//@flow
import React from 'react';
import { Query, Mutation } from 'react-apollo';
import { ALUMNOSQUERY, ADD_ALUMNOS, } from './queries';
import DeleteAlumno from './delete';
import AddPerfil from '../Perfil/addprofile';
import _ from 'lodash';
import { Grid, Typography, CircularProgress, Button } from '@material-ui/core';
import { alumnos } from './constants';
import { useQuery } from '@apollo/react-hooks';
import { Row } from './styledCompontes';

const Alumnos = () => {
    const filterByCurp = (correo, data) => {
        // let curp = correo.replace('@sepyc.edu.mx', '')
        return _.filter(data, { correo: correo })
    }
    const filterByCurp2 = (correo) => {
         const curp = correo.replace('@sepyc.edu.mx', '')
        return _.filter(alumnos, { curpAlumno: curp })
    }
    const setAlumnos = (addAlumnos, data) => {
        alumnos.forEach(element => {
            if (filterByCurp(`${element.curpAlumno}@sepyc.edu.mx`, data).length === 0) {
                addAlumnos({
                    variables: {
                        apellidoMaterno: element.maternoAlumno,
                        apellidoPaterno: element.paternoAlumno,
                        correo: `${element.curpAlumno}@sepyc.edu.mx`,
                        nombre: element.nombreAlumno,
                    }, refetchQueries: [{ query: ALUMNOSQUERY }]
                })

            } else {
                console.log("error, curp repetida")
            }

        });
    }
    /*    const getCheckedArray = (array) =>{
           let checkedArray = [];
           array.forEach(element => {
               checkedArray.push(false)
           });
       } */
    return <div>

        <Query query={ALUMNOSQUERY}>

            {({ loading, error, data }) => {
                if (loading) return <CircularProgress color="primary" />;
                if (error) return (console.log(error));
                if (data) {
                    return <>
                        <Mutation mutation={ADD_ALUMNOS}>
                            {(addTodo) => (
                                <Button variant="contained" color="primary" style={{ marginBottom: '1%', backgroundColor: '#38B549' }} onClick={() => setAlumnos(addTodo, data)}>Agregar alumnos</Button>
                            )}
                        </Mutation>
                        <Grid container style={{ backgroundColor: '#F2F2F2' }}>
                            <Grid container item xs={1} justify="center"><Typography><strong>id</strong></Typography></Grid>
                            <Grid container item xs={1} justify="center"><Typography><strong>A. Paterno</strong></Typography></Grid>
                            <Grid container item xs={1} justify="center"><Typography><strong>A. Materno</strong></Typography></Grid>
                            <Grid container item xs={2} justify="center"><Typography><strong>Nombre</strong></Typography></Grid>
                            <Grid container item xs={3} justify="center"><Typography><strong>Correo</strong></Typography></Grid>
                        </Grid>
                        {data.usuarios.map((element) => (
                            <Row container alignItems="center" key={element.id} style={{ marginBottom: '1%' }}>
                                <Grid container item xs={1} justify="center"><Typography key={`${element.id}id`}>{element.id}</Typography></Grid>
                                <Grid container item xs={1} justify="center"><Typography key={`${element.apellido_paterno}apellido_paterno`}>{element.apellido_paterno}</Typography></Grid>
                                <Grid container item xs={1} justify="center"><Typography key={`${element.apellido_materno}apellido_materno`}>{element.apellido_materno}</Typography></Grid>
                                <Grid container item xs={2} justify="center"><Typography key={`${element.nombre}nombre`}>{element.nombre}</Typography></Grid>
                                <Grid container item xs={3} justify="center"><Typography key={`${element.id}correo`}>{element.correo}</Typography></Grid>
                                <Grid item xs={1}>
                                    <DeleteAlumno id={element.id} key={`${element.id}delete`}></DeleteAlumno>
                                </Grid>
                                <Grid container item xs={1} justify="center">
                                    <AddPerfil
                                        id={element.id}
                                        clave={
                                            filterByCurp2(element.correo).length !== 0 ? (filterByCurp2(element.correo)[0].claveCT) :null
                                        }
                                        nombreGrupo={
                                            filterByCurp2(element.correo).length !== 0 ? (filterByCurp2(element.correo)[0].claveGrupo) :null
                                        }
                                        grado={
                                            filterByCurp2(element.correo).length !== 0 ? (filterByCurp2(element.correo)[0].claveGrado) :null
                                        }
                                    ></AddPerfil>
                                </Grid>
                            </Row>
                        ))}
                    </>
                }
                else {
                    return (
                        <div> get some work boi...</div>
                    )
                }
            }}
        </Query>
    </div>;
};

export default (Alumnos);
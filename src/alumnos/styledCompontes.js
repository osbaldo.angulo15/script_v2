import styled from 'styled-components';
import {Grid} from '@material-ui/core';

export const Row = styled(Grid)`
    &:hover{
        background-color: #EBF7DE;
    }
`;
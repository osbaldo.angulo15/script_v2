export const alumnos = [
	{
		"No_registro": "5119",
		"curpAlumno": "AERL121125MSRRLSA3",
		"paternoAlumno": "ARMENTA",
		"maternoAlumno": "RUELAS",
		"nombreAlumno": "LESLI VIOLETA",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
]
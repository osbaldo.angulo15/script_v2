import gql from 'graphql-tag';

export const DOMICILIOSQUERY = gql`
query MyQuery {
  __typename
  domicilios(where: {usuario_id: {_is_null: false}, _and: {calle: {_eq: "0"}}}) {
    id
    calle
    usuario_id
    codigo_postal
    lat
    lon
  }
}
`;

export const DELETE_DOMICILIOS_BY_USUARIO_ID = gql`
mutation deletePerfilByUsuarioId($usuarioId: Int!) {
  delete_domicilios(
    where: {usuario_id: {_eq: $usuarioId}})
  {
    affected_rows
  }
}
`;
export const ALUMNOSQUERY = gql`
    query {
        usuarios {
            id
            apellido_materno
            apellido_paterno
            correo
            fecha_registro
            registrado_por
            nombre
        }
    }
  
`;
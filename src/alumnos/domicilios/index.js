//@flow
import React from 'react';
import { Query} from 'react-apollo';
import { DOMICILIOSQUERY} from './queries';
// import DeleteAlumno from './delete';
import { Typography, Grid } from '@material-ui/core';
import { Row } from '../styledCompontes';

const Domicilios = () => {
    return <div>
            <Typography variant="h4"><strong>DOMICILIOS</strong></Typography>
            <Grid container style={{marginBottom: '1%', backgroundColor: '#F2F2F2'}}>
                <Grid container item xs={1} justify="center"><Typography>id</Typography></Grid>
                <Grid container item xs={1} justify="center"><Typography>usuario_id</Typography></Grid>
                <Grid container item xs={1} justify="center"><Typography>calle</Typography></Grid>
                <Grid container item xs={1} justify="center"><Typography>lat</Typography></Grid>
                <Grid container item xs={1} justify="center"><Typography>lon</Typography></Grid>
            </Grid>
        <Query query={DOMICILIOSQUERY}>
            {({ loading, error, data }) => {
                if (loading) return "Loading...";
                if (error) return (console.log(error));
                if (data) {
                    return data.domicilios.map((element) => (
                        <Row container key={element.id} style={{marginBottom: '1%'}}>
                            <Grid container item xs={1} justify="center" alignItems="center"><Typography key={`${element.id}`}>{element.id}</Typography></Grid>
                            <Grid container item xs={1} justify="center" alignItems="center"><Typography key={`${element.usuario_id}`}>{element.usuario_id}</Typography></Grid>
                            <Grid container item xs={1} justify="center" alignItems="center"><Typography key={`${element.calle}`}>{element.calle}</Typography></Grid>
                            <Grid container item xs={1} justify="center" alignItems="center"><Typography key={`${element.lat}`}>{element.lat}</Typography></Grid>
                            <Grid container item xs={1} justify="center" alignItems="center"><Typography key={`${element.lon}`}>{element.lon}</Typography></Grid>
                           {/*  <DeleteAlumno id={element.id}/> */}
                        </Row>
                    ));
                }
                else {
                    return (
                        <div> get some work boi...</div>
                    )
                }
            }}
        </Query>
    </div>;
};

export default (Domicilios);
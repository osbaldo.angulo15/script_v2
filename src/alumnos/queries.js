import gql from 'graphql-tag';

export const ALUMNOSQUERY = gql`
    query {
        usuarios {
            id
            apellido_materno
            apellido_paterno
            correo
            fecha_registro
            registrado_por
            nombre
        }
    }
  
`;

export const ADD_ALUMNOS = gql`
mutation addAlumnos(
    $apellidoMaterno: String!,
    $apellidoPaterno: String!,
    $correo: String!,
    $nombre: String!,
    ) {
    insert_usuarios(objects: [{
        apellido_materno: $apellidoMaterno,
        apellido_paterno: $apellidoPaterno,
        correo: $correo,
        nombre: $nombre,
        fecha_registro: "2019-10-23",
        id_tipo_usuario: 2,
    }]){
      returning{
        id
      }
    }
}
`;
export const DELETE_ALUMNOS = gql`
mutation deleteAlumnos($id: Int!) {
    delete_usuarios(
        where: {id: {_eq: $id}})
      {
        affected_rows
      }
}

`;

export const DELETE_PERFIL_BY_USUARIO_ID = gql`
mutation deletePerfilByUsuarioId($usuarioId: Int!) {
  delete_perfil(
    where: {usuario_id: {_eq: $usuarioId}})
  {
    affected_rows
  }
}
`;